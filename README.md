# ArgoCD Deployment for MovieApp

## Описание

Этот репозиторий содержит конфигурации Helm Chart и Application для автоматического деплоя приложения MovieApp в Kubernetes с использованием ArgoCD.

## Содержимое репозитория

- `charts/` - Директория с Helm Chart для деплоя компонентов MovieApp.
- `applications.yml` - Файл с ArgoCD Application манифестами.